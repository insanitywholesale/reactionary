# build environment
FROM node:alpine as build
WORKDIR /app
COPY . /app
RUN npm install
RUN npm install react-scripts -g
RUN npm run build

# production environment
FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
