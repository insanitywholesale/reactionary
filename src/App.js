import React, { Component } from 'react';
import NavBar from './components/navbar';
import Counters from './components/counters';
import './App.css';

class App extends Component {
	state = {
		counters: [
			{id : 1, value: 0},
			{id : 2, value: 0},
			{id : 3, value: 0},
			{id : 4, value: 0},
			{id : 5, value: 0},
		]
	}

	handleReset = () => {
		const counters = this.state.counters.map(c => {c.value = 0; return c; } );
		this.setState({ counters });
	}

	handleDelete = (counterId) => {
		const counters = this.state.counters.filter(c => c.id !== counterId);
		this.setState({ counters });
	}
	
	handleIncrement = (counter) => {
		const counters = [...this.state.counters];
		const i = counters.indexOf(counter);
		counters[i] = {...counter};
		counters[i].value++;
		this.setState({ counters });
	}

	render() {
		return (
			<React.Fragment>
			<NavBar
				totalUniqueCounters={this.state.counters.filter(c => c.value > 0).length}
				totalCounters={this.state.counters.map(c => c.value).reduce((total, amount) => total + amount, 0)}/> {/*get sum of non-zero value counters*/}
				<main className="container">
					<Counters
						counters={this.state.counters}
						onIncrement={this.handleIncrement}
						onDelete={this.handleDelete}
						onReset={this.handleReset}
					/>
				</main>
			</React.Fragment>
		);
	}

}

export default App;
