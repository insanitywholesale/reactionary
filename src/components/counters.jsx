import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {

	render () {
		const {counters, onIncrement, onDelete, onReset} = this.props;
		return (
			<div>
				<button
					onClick={onReset}
					className="btn btn-primary btn-sm m-2">
						Reset {/*button label*/}
				</button>
				{counters.map(counter => 
					<Counter
						counter={counter}
						key={counter.id}
						onIncrement={onIncrement}
						onDelete={onDelete}
					/>)
				}
			</div>
		);
	}

}

export default Counters;
