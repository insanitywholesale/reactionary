import React, { Component } from 'react';

class Counter extends Component {
	state = {
		count: 0,
		tags: ['tag', 'tag1', 'tag2', 'tag3']
	};

/*Worse alternative to handleIncrement = () => {}
	constructor() {
		super();
		this.handleIncrement = this.handleIncrement.bind(this);
	}
*/

	render () {
		return (
			<div>
				<code>var bloat = this.thing(here);</code>
				<p></p>
				<button onClick={this.handleIncrement} className="btn btn-secondary btn-sm">Fucky Wucky Clicky</button>
				<p></p>
				<i>has been clicked:</i>
				<p></p>
				<span className={this.getBadgeClasses()}>{this.formatCount()}</span>
				<p></p>
				<i>times</i>
				<ul>
					<li>show a list of items</li>{ this.state.tags.map(tag => <li key={tag}>{tag}</li>) }
					{/*this is the JSX equivalent of a {% for %} in jinja templates*/}
				</ul>
			</div>
		);
	}

	handleIncrement= () => {
		this.setState({ count: this.state.count + 1 });
	}

	getBadgeClasses() {
		let classes = "badge m-2 badge-";
		classes += (this.state.count === 0) ? "warning" : "primary"; /*resolves to badge-warning or badge-primary*/
		return classes;
	}

	formatCount() {
		const { count } = this.state;
		return count === 0 ? 'Zero' : count;
	}

}

export default Counter;
