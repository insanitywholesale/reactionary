import React from 'react';

const NavBar = ({ totalUniqueCounters, totalCounters }) => {
	return (
		<nav className="navbar navbar-light bg-light">
		<span>
		  <a className="navbar-brand" href="#navlink">
			Total of Unique Items: {/* navbar label*/}
			<span className="badge badge-pill badge-secondary">
				{totalUniqueCounters}
			</span>
		</a>
	 	<a className="navbar-brand" href="#navlink">
			Total Items: {/* navbar label*/}
			<span className="badge badge-pill badge-secondary">
				{totalCounters}
			</span>
		</a>
		</span>
		</nav>
	);

}

export default NavBar;
