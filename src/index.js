import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css'; //addition
import App from './App';
import * as serviceWorker from './serviceWorker';

var rootBoi = document.getElementById('root');
ReactDOM.render(<App />, rootBoi);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
